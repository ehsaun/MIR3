import numpy as np
from scipy.linalg import eig


class PageRank:
    def __init__(self, matrix, alfa):
        self.matrix = matrix
        self.alpha = alfa
        self.n = len(matrix)
        self.normalize()
        self.add_teleport()
        # # self.matrix.astype(float128)
        # arsh = np.array([0.3100155, 0.1749272,   0.13787938, 0.25357298, 0.12360494     ])
        self.calc_rank()


    def get_rank(self):
        return self.rank

    def normalize(self):
        self.matrix = np.array(self.matrix)
        l = np.array([n if n else 1 for n in np.linalg.norm(self.matrix, axis=1, ord=1)])
        self.matrix = self.matrix / l[:, np.newaxis]

    def add_teleport(self):
        self.matrix = self.matrix * (1 - self.alpha) + (self.alpha/self.n)

    def calc_rank(self):
        self.rank = eig(self.matrix, left=True, right=False)[1][:, 0]
        self.rank /= np.sum(np.abs(self.rank))
        self.rank = list(np.abs(self.rank.real))



if __name__ == '__main__':
    """test pageRank class"""
    print('testing page rank')
    a = [[1, 1, 1, 1, 0],
         [1, 1, 1, 0, 0],
         [1, 1, 0, 0, 0],
         [1, 0, 0, 0, 0],
         [0, 0, 0, 0, 0]]
    print(PageRank(a, 0.1).get_rank())