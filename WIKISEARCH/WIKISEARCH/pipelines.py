# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import shutil
import json
import os
from tqdm import tqdm


class WikisearchJsonWriterPipeline(object):

    def open_spider(self, spider):
        if os.path.exists('jsons/'):
            shutil.rmtree('jsons/')
        os.makedirs(os.path.dirname('jsons/'))
        self.pbar = tqdm(total=spider.capacity)


    def process_item(self, item, spider):

        with open('jsons/' + str(item['pk']) + '- ' + item['title'] + '.json', 'w') as fp:
            json.dump(item, fp)
        self.pbar.update(1)
        return item


    def close_spider(self, spider):
        self.pbar.close()