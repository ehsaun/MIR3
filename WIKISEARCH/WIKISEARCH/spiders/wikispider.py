# -*- coding: utf-8 -*-
from scrapy.crawler import CrawlerProcess
import scrapy
import re
from scrapy.utils.project import get_project_settings


class WikispiderSpider(scrapy.Spider):
    name = "wikispider"
    allowed_domains = ["fa.wikipedia.org"]
    start_urls = ['']

    def __init__(self, capacity=1000, out_degree=10, start_urls=['https://fa.wikipedia.org/wiki/%D9%85%D9%86%D8%B7%D9%82']):
        self.capacity = capacity
        self.out_degree = out_degree
        self.start_urls = start_urls
        self.seen_urls = set(start_urls)
        self.counter = 0
        # self.base = self.counter

    custom_settings = {
        'DEPTH_PRIORITY': 1,
        'SCHEDULER_DISK_QUEUE': 'scrapy.squeues.PickleFifoDiskQueue',
        'SCHEDULER_MEMORY_QUEUE': 'scrapy.squeues.FifoMemoryQueue',
        'CONCURRENT_REQUESTS_PER_DOMAIN': 1,
        # 'LOG_ENABLED': 0,
        # 'lOG_LEVEL': 'ERROR',
        # 'CLOSESPIDER_PAGECOUNT': 1,
        'ITEM_PIPELINES': {'WIKISEARCH.WIKISEARCH.pipelines.WikisearchJsonWriterPipeline': 300,},
    }

    def parse(self, response):
        # print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$4', self.counter)
        if self.counter >= self.capacity: return
        self.counter += 1
        main = response.xpath("descendant-or-self::div[@id = 'mw-content-text']/h2[span[contains(text(), 'پانویس') or contains(text(), 'منابع') or contains(text(), 'مراجع') or contains(text(), 'پیوند به بیرون')]][1]/preceding-sibling::*[not(self::div or self::script or self//@class = 'plainlinks') and count(preceding-sibling::p)>=1 or self::p]")
        urls = [u.css('::attr(href)').extract_first() for u in main.css('a') if not (u.css('::attr(class)').extract_first() in ['image'] or u.xpath('normalize-space(.)').extract_first() in ['ویرایش'] or any(c.isdigit() or c in ':[]' for c in u.xpath('normalize-space(.)').extract_first()))]
        urls = [response.urljoin(u) for u in urls if not (u.startswith('https://') or any(c in u for c in '#:.'))]

        info = {
            'pk': self.counter,
            'url': response.url,
            'title': response.xpath('//*[(@id = "firstHeading")]//text()').extract_first(),
            'introduction': (re.sub(r'\[.*?\]', '', response.xpath("normalize-space(descendant-or-self::div[@id = 'mw-content-text']/p/*[1][self::b]/..)").extract_first())),
            'contents': (re.sub(r'\[.*?\]', '', ' '.join(e.xpath('normalize-space(.)').extract_first() for e in main))),
            'infobox': (response.xpath("normalize-space(descendant-or-self::table[contains(@class, 'infobox')])").extract_first()),
            'links': urls if urls else ['wth'],
        }
        yield info
        # print(info['contents'])

        # # print('############################################4', self.counter, response.xpath('//*[(@id = "firstHeading")]//text()').extract_first())

        for u in urls[:self.out_degree]:
            if u not in self.seen_urls:
                self.seen_urls.add(u)
                yield scrapy.Request(u, callback=self.parse)


def run(capacity, out_degree, start_urls):
    settings = get_project_settings()
    settings.set('LOG_ENABLED', False)
    start_urls = start_urls if start_urls else ['https://fa.wikipedia.org/wiki/%D8%B3%D8%B9%D8%AF%DB%8C']
    process = CrawlerProcess(settings)
    process.crawl(WikispiderSpider, capacity, out_degree, start_urls)
    process.start()
